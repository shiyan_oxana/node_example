var mysqlPromise = require('mysql-promise')(),
    config = require('../config/' + (process.env.NODE_ENV || 'development'));

mysqlPromise.configure(config.db.mysql);

function checkMySQLConnection(){            // [2]
    return mysqlPromise.query('SELECT 1');
}

function init() {                           // [4]
    return Promise.all([
        checkMySQLConnection()

    ]);
}

function getArticles() {
    return mysqlPromise.query('SELECT * from `articles`');
}
function getArticleOne(id) {
    return mysqlPromise.query('SELECT * from `articles` WHERE `id`='+id+'')
}

module.exports = {                          // [5]
    mysql: mysqlPromise,
    init:  init,
    getArticles: getArticles,
    getArticleOne: getArticleOne
};