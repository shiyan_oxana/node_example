var config = {
    db : {
        mysql : {
            host     : 'localhost',
            user     : 'root',
            database : 'node', // можете заменить 'appdb' на свое название
            password : '' // замените это на root пароль
        },                                // от MySQL Server
    },                                        // на свое название
    port : 3000
};
module.exports =  config;
