 var Article = require('../models/articles');
 exports.all = function(req, res){
     Article.all(function(err, docs){
         if(err){
             console.log(err);
             return res.sendStatus(500);
         }
         //console.log(docs);
         docs.forEach(function(index, value){
             console.log(value);
             console.log(index.name);
         });
         //res.send(docs);
         res.render('blog', { title: 'Articles', articles: docs });

     });
 }
 exports.get = function(req, res){
    Article.get(req.params.id,function(err, article){
         if(err){
             console.log(err);
             return res.sendStatus(500);
         }
         console.log(JSON.parse(JSON.stringify(article)));
         res.render('article', { title: ' Edit article 2', article: JSON.parse(JSON.stringify(article[0])) });
    });
 }