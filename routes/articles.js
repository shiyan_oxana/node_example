var express = require('express');
var router = express.Router();
var articleController = require('../controllers/articles');

/* GET users listing. */
/*router.get('/', function(req, res, next) {
 // res.send('respond with a resource article');

});*/
router.get('/', articleController.all);

router.get('/:id', articleController.get);
/*router.get('/:id', function(req, res, next) {
    res.render('index', { title: 'Express' });
});*/

module.exports = router;
