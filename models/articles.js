var config = require('../config/' + (process.env.NODE_ENV || 'development')),
    dbs = require('../libs/dbs');

exports.all = function(cb){
  dbs.getArticles().then(function(rows){
      cb(null, rows[0]);
  }).catch((e) => {
      console.log('error: ', e)
});
}
exports.get = function(id,cb){
    dbs.getArticleOne(id).then(function(rows){
        cb(null, rows[0]);
    }).catch((e) => {
        console.log('error: ', e)
    });
}
